Source: vspline
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Kay F. Jahnke <kfjahnke@gmail.com>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/vspline
Vcs-Git: https://salsa.debian.org/science-team/vspline.git
Homepage: https://bitbucket.org/kfj/vspline

Package: vspline-dev
Architecture: all
Depends: libvigraimpex-dev,
         ${misc:Depends}
Suggests: clang,
          vc-dev,
          libhwy-dev
Description: header-only C++ template library for b-spline processing
 vspline aims to be as fast as possible, yet mathematically precise.
 It's main focus is interpolation of bulk data like sounds, images and
 volumes. vspline can handle
 .
  -  splines over real and integer data types and their aggregates:
  -  all 'xel' data, arbitrary number of channels (template argument)
  -  single, double precision and long doubles supported (template argument)
  -  splines using integral coefficients
  -  a reasonable selection of boundary conditions
  -  arbitrary dimensionality of the spline (template argument)
  -  spline degree up to 45 (runtime argument)
  -  there is specialized code for 1D data
  -  'transform' family functions use multithreaded code (pthread)
  -  optional use of the CPU's vector units (like AVX/2, AVX512f, ARM NEON)
  -  with choice of SIMD back-end (Vc, highway, std::simd, 'goading')
 .
 On the evaluation side it provides
 .
  -  evaluation of the spline at point locations in the defined range
  -  evaluation of vectorized arguments
  -  evaluation of the spline's derivatives
  -  factory functions to create evaluation functors
  -  specialized code for degrees 0 and 1 (nearest neighbour and n-linear)
  -  mapping of arbitrary coordinates into the defined range
  -  evaluation of nD arrays of coordinates ('remap' function)
  -  discrete-coordinate-fed remap function ('index_remap')
  -  generalized functor-based 'apply' and 'transform' functions
  -  restoration of the original data from the spline coefficients
 .
 To produce maximum performance, vspline has a fair amount of collateral code,
 and some of this code may be helpful beyond vspline:
 .
  -  multithreading with a thread pool
  -  efficient processing of nD arrays with multiple threads
  -  functional constructs using vspline::unary_functor
  -  nD forward-backward n-pole recursive filtering
  -  nD separable convolution
  -  efficient access to b-spline basis functions and their derivatives
  -  precise precalculated constants (made with GNU GSL, BLAS and GNU GMP)
  -  use of alternative basis functions
  -  common interface to several SIMD back-ends
  -  vigra type traits for the SIMD data types
  -  many examples, ample comments
 .
 vspline uses a three-step approach to splines: the first step is coefficient
 generation (including prefilering), the second step is generation of specific
 functors providing evaluation, the third is application of these functors
 to single values, SIMD vectors or n-dimensional raster data.
 data handling is done with vigra data types, using vigra::MultiArrayView
 for handling strided nD arrays, and vigra::TinyVector for small aggregates.
 vigra::MultiArrayView is a thin wrapper combining an array's address,
 dimension, data type, stride and shape, it's easy to use it to 'package'
 raw data. vspline handles vector formation from interleaved data, peeling
 and re-interleaving automatically.
 vspline optionally uses Vc, highway or std::simd. Without any of the SIMD
 back-ends present, it triggers the compiler's autovectorization by producing
 deliberately vector-friendly inner loops.
 vspline has been under development for several years and is extensively
 tested for scope coverage, reliability, precision and performance.
