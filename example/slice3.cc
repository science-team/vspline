/************************************************************************/
/*                                                                      */
/*    vspline - a set of generic tools for creation and evaluation      */
/*              of uniform b-splines                                    */
/*                                                                      */
/*            Copyright 2015 - 2023 by Kay F. Jahnke                    */
/*                                                                      */
/*    Permission is hereby granted, free of charge, to any person       */
/*    obtaining a copy of this software and associated documentation    */
/*    files (the "Software"), to deal in the Software without           */
/*    restriction, including without limitation the rights to use,      */
/*    copy, modify, merge, publish, distribute, sublicense, and/or      */
/*    sell copies of the Software, and to permit persons to whom the    */
/*    Software is furnished to do so, subject to the following          */
/*    conditions:                                                       */
/*                                                                      */
/*    The above copyright notice and this permission notice shall be    */
/*    included in all copies or substantial portions of the             */
/*    Software.                                                         */
/*                                                                      */
/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND    */
/*    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   */
/*    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          */
/*    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       */
/*    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      */
/*    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      */
/*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     */
/*    OTHER DEALINGS IN THE SOFTWARE.                                   */
/*                                                                      */
/************************************************************************/

/// \file slice3.cc
///
/// \brief create 2D image data from a 3D spline
///
/// build a 3D volume from samples of the RGB colour space
/// build a spline over it and extract a 2D slice
///
/// Here we use a quick shot solution.
/// Oftentimes all it takes is a single run of an interpolation with
/// as little programming effort as possible, never mind the performance.
/// Again we use a b-spline with double-precision voxels as value_type,
/// but instead of using vspline::transform, we simply run the calculations
/// in loops.
///
/// compile with:
/// clang++ -std=c++11 -o slice3 -O3 -pthread slice3.cc -lvigraimpex
/// g++ also works.

#include <iostream>

#include <vspline/vspline.h>

#include <vigra/stdimage.hxx>
#include <vigra/imageinfo.hxx>
#include <vigra/impex.hxx>

// voxel_type is the source data type
typedef vigra::RGBValue < double , 0 , 1 , 2 > voxel_type ;

// pixel_type is the result type, here we use a vigra::RGBValue for a change
typedef vigra::RGBValue < unsigned char , 0 , 1 , 2 > pixel_type ;

// coordinate_type has a 3D coordinate
typedef vigra::TinyVector < float , 3 > coordinate_type ;

int main ( int argc , char * argv[] )
{
  // we want a b-spline with natural boundary conditions
  vigra::TinyVector < vspline::bc_code , 3 > bcv ( vspline::NATURAL ) ;
  
  // create quintic 3D b-spline object containing voxels
  vspline::bspline < voxel_type , 3 >
    bspl ( vigra::Shape3 ( 10 , 10 , 10 ) , 5 , bcv ) ;
  
  // fill the b-spline's core with a three-way gradient
    
  for ( int z = 0 ; z < 10 ; z++ )
  {
    for ( int y = 0 ; y < 10 ; y++ )
    {
      for ( int x = 0 ; x < 10 ; x++ )
      {
        bspl.core ( x , y , z ) = voxel_type ( 25.5 * x , 25.5 * y , 25.5 * z ) ;
      }
    }
  }
  
  // prefilter the b-spline
  
  bspl.prefilter() ;
  
  // get an evaluator for the b-spline
  
  auto ev = vspline::make_evaluator ( bspl ) ;
  
  // this is where the result should go:
  
  vigra::MultiArray < 2 , pixel_type > target ( vigra::Shape2 ( 1920 , 1080 ) ) ;

  // we want the pick-up coordinates to follow this scheme:
  // pick ( x , y ) = ( x , 1 - x , y )
  // scaled appropriately
  
  for ( int y = 0 ; y < 1080 ; y++ )
  {
    for ( int x = 0 ; x < 1920 ; x++ )
    {
      // calculate the pick-up coordinate
      coordinate_type pick { x / 192.0f ,
                             10.0f - x / 192.0f ,
                             y / 108.0f } ;
      // call the evaluator and store the result to 'target'                   
      target ( x , y ) = ev ( pick ) ;
    }
  }
  
  // store the result with vigra impex
  vigra::ImageExportInfo imageInfo ( "slice.tif" );
  
  vigra::exportImage ( target ,
                      imageInfo
                      .setPixelType("UINT8")
                      .setCompression("100")
                      .setForcedRangeMapping ( 0 , 255 , 0 , 255 ) ) ;
  
  std::cout << "result was written to slice.tif" << std::endl ;
  exit ( 0 ) ;
}
